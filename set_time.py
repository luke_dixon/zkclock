#!/usr/bin/env python
"""
Connect to the timeclock and set the time.
"""
from __future__ import unicode_literals
from datetime import datetime
try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser
import logging

import zkclock


logging.basicConfig(level=logging.INFO)

USHRT_MAX = 65535


if __name__ == "__main__":

    # IP address and port numbers for the timeclock we are connecting to
    parser = ConfigParser()
    parser.read('config.ini')
    host = parser.get('zkclock', 'host')
    port = parser.getint('zkclock', 'port')
    password = parser.get('zkclock', 'passcode')

    s, r = zkclock.connect(host, port, password)
    r = zkclock.disable(s, r)

    # set the time
    r = zkclock.set_time(s, r, datetime.now())

    r = zkclock.refresh_config_data(s, r)
    r = zkclock.disconnect(s, r)
