#!/usr/bin/env python
"""
Write the data retrieved from the timeclock to a spreadsheet
"""
from __future__ import unicode_literals
import pickle
from datetime import date
import logging
import xlwt

import zkclock


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

LOG_DATA_SIZE = 8
#LOG_DATA_SIZE = 39


if __name__ == "__main__":

    # Set the date we want to make a spread sheet for
    TODAY = date.today()
    #TODAY = date(2010, 6, 30)

    # Open and load the data we saved in get_attendance_log.py
    with open('pickle-file', 'rb') as f:
        userdata, logdata = pickle.load(f)

    logdata = zkclock.assemble_log_data_from_packets(logdata)

    # Create a dictionary of user id s to user names
    users = zkclock.unpack_users_to_dict(userdata)
    logging.debug(users)

    # Split the attendance log data into a list of each entry
    loglist = []
    while((len(logdata) / LOG_DATA_SIZE) >= 1):
        loglist.append(logdata[:LOG_DATA_SIZE])
        logdata = logdata[LOG_DATA_SIZE:]

    log = []
    for i in loglist:
        log_entry = zkclock.unpack_log(i)
        timestamp = zkclock.decode_time(log_entry.time)
        verification_method = log_entry.decode_verified()
        try:
            user_name = users[log_entry.uid].name
        except KeyError:
            user_name = "Unknown user: %s" % log_entry.uid
        log.append([timestamp, user_name, verification_method])

    # Open a spreadsheet and make a header row
    wb = xlwt.Workbook()
    ws = wb.add_sheet("Sheet 1")
    headers = ["Time", "ID", "In/Out"]
    for i in range(len(headers)):
        ws.write(0, i, headers[i])

    row_number = 0
    # for each log entry
    for i in range(len(log)):
        # if the date matches
        if log[i][0].date() == TODAY:
            # copy this entry into a row
            row = [str(log[i][0]), log[i][1].decode('utf-8'), log[i][2]]
            for j in range(len(row)):
                logging.debug(log[i][j])
                ws.write(row_number + 1, j, row[j])
            # move to next row
            row_number += 1

    # Save to a spreadsheet with today's date
    wb.save("%s.xls" % TODAY)
