import unittest
from datetime import datetime

from zkclock import commands


class TimeTest(unittest.TestCase):
    datetime_values = (
        (datetime(2000, 1, 1, 0, 0, 0), 0),
        (datetime(2005, 5, 5, 5, 5, 5), 171781505),
        (datetime(2013, 2, 28, 23, 59, 59), 422927999),
        (datetime(2012, 2, 29, 13, 9, 30), 390834570),
        (datetime(2099, 12, 31, 23, 59, 59), 3214079999),
    )

    def test_encode_time(self):
        for datetime_value in self.datetime_values:
            self.assertEqual(
                commands.encode_time(datetime_value[0]),
                datetime_value[1])

    def test_decode_time(self):
        for datetime_value in self.datetime_values:
            self.assertEqual(
                commands.decode_time(datetime_value[1]),
                datetime_value[0])
