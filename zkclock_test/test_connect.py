import unittest
try:
    from unittest import mock
except ImportError:
    import mock
import struct
import logging

import zkclock


logging.basicConfig(level=logging.INFO)


class ConnectionTest(unittest.TestCase):
    def test_connect(self):
        # Mock all the received packets
        mock_socket = mock.Mock()
        expected_replies = [
            struct.pack('HHHH', zkclock.CMD_ACK_OK, 1, 0, 0),
        ]
        mock_socket.recv.side_effect = expected_replies
        zkclock.socket.socket = mock.Mock()
        zkclock.socket.socket.return_value = mock_socket

        # Call what we are testing
        socket, reply = zkclock.connect('localhost', 4317)

        # Check return values
        self.assertEqual(socket, mock_socket)
        self.assertEqual(reply, expected_replies[0])

        # check calls
        expected = [
            mock.call(struct.pack('HHHH', zkclock.CMD_CONNECT, 64535, 0, 0)),
        ]
        self.assertEqual(mock_socket.send.mock_calls, expected)

    def test_connect_auth(self):
        # Mock all the received packets
        mock_socket = mock.Mock()
        expected_replies = [
            struct.pack('HHHH', zkclock.CMD_ACK_UNAUTH, 33032, 30498, 0),
            struct.pack('HHHH', zkclock.CMD_ACK_OK, 33036, 30498, 1),
        ]
        mock_socket.recv.side_effect = expected_replies
        zkclock.socket.socket = mock.Mock()
        zkclock.socket.socket.return_value = mock_socket

        # Call what we are testing
        socket, reply = zkclock.connect('localhost', 4317, 123456)

        # Check return values
        self.assertEqual(socket, mock_socket)
        self.assertEqual(reply, expected_replies[1])

        # check calls
        expected = [
            mock.call(struct.pack('HHHH', zkclock.CMD_CONNECT, 64535, 0, 0)),
            mock.call(struct.pack(
                'HHHHBBBB',
                zkclock.CMD_AUTH, 30517, 30498, 1, 38, 127, 50, 142)),
        ]
        self.assertEqual(mock_socket.send.mock_calls, expected)
