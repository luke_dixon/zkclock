"""
commands.py

Has a bunch of functions that assemble the data packet to be sent to the
time and attendance machine.
"""

from __future__ import unicode_literals
from datetime import datetime
import socket
from struct import pack, unpack
import logging


logger = logging.getLogger(__name__)

USHRT_MAX = 65535


def make_commkey(key, session_id, ticks=50):
    """take a password and session_id and scramble them to send to the time
    clock.
    copied from commpro.c - MakeKey"""
    key = int(key)
    session_id = int(session_id)
    k = 0
    for i in range(32):
        if (key & (1 << i)):
            k = (k << 1 | 1)
        else:
            k = k << 1
    k += session_id

    k = pack(b'I', k)
    k = unpack(b'BBBB', k)
    k = pack(
        b'BBBB',
        k[0] ^ ord('Z'),
        k[1] ^ ord('K'),
        k[2] ^ ord('S'),
        k[3] ^ ord('O'))
    k = unpack(b'HH', k)
    k = pack(b'HH', k[1], k[0])

    B = 0xff & ticks
    k = unpack(b'BBBB', k)
    k = pack(
        b'BBBB',
        k[0] ^ B,
        k[1] ^ B,
        B,
        k[3] ^ B)
    return k


def in_chksum(p):
    """This function calculates the chksum of the packet to be sent to the
    time clock

    Copied from zkemsdk.c"""
    l = len(p)
    chksum = 0
    while l > 1:
        chksum += unpack(b'H', pack(b'BB', p[0], p[1]))[0]
        p = p[2:]
        if chksum > USHRT_MAX:
            chksum -= USHRT_MAX
        l -= 2
    if l:
        logger.debug(l)
        chksum = chksum + p[-1]
    while chksum > USHRT_MAX:
        chksum -= USHRT_MAX
    chksum = ~chksum
    while chksum < 0:
        chksum += USHRT_MAX
    return pack(b'H', chksum)


def construct_header(command, chksum, session_id, reply_id, command_bytes):
    """This function puts a the parts that make up a packet together and
    packs them into a byte string"""
    buf = pack(b'HHHH', command, chksum, session_id, reply_id) + command_bytes

    logger.debug("Command String Length: {l}".format(l=len(command_bytes)))
    buf = unpack('8B{l}B'.format(l=len(command_bytes)).encode('utf-8'), buf)

    chksum = unpack(b'H', in_chksum(buf))[0]
    reply_id += 1
    if reply_id >= USHRT_MAX:
        reply_id -= USHRT_MAX

    buf = pack(b'HHHH', command, chksum, session_id, reply_id)
    return buf + command_bytes


# These commands can be found in the technical manual or cmds.h
CMD_CONNECT = 1000
CMD_EXIT = 1001
CMD_ENABLEDEVICE = 1002
CMD_DISABLEDEVICE = 1003
CMD_GET_VERSION = 1100
CMD_AUTH = 1102
CMD_REFRESHDATA = 1013

CMD_ACK_OK = 2000
CMD_ACK_ERROR = 2001
CMD_ACK_DATA = 2002
CMD_ACK_UNAUTH = 2005

CMD_PREPARE_DATA = 1500
CMD_DATA = 1501

CMD_USER_WRQ = 8
CMD_USERTEMP_RRQ = 9
CMD_USERTEMP_WRQ = 10
CMD_ATTLOG_RRQ = 13
CMD_GET_TIME = 201
CMD_SET_TIME = 202

FCT_ATTLOG = 1
FCT_FINGERTMP = 2
FCT_OPLOG = 4
FCT_USER = 5
FCT_SMS = 6
FCT_UDATA = 7
FCT_WORKCODE = 8


def encode_time(t):
    """Encode a timestamp so that it can be read on the timeclock
    """
    # formula taken from zkemsdk.c - EncodeTime
    # can also be found in the technical manual
    d = (
        ((t.year % 100) * 12 * 31 + ((t.month - 1) * 31) + t.day - 1) *
        (24 * 60 * 60) + (t.hour * 60 + t.minute) * 60 + t.second
    )
    return d


def decode_time(t):
    """Decode a timestamp retrieved from the timeclock
    """
    # copied from zkemsdk.c - DecodeTime
    second = t % 60
    t = t // 60

    minute = t % 60
    t = t // 60

    hour = t % 24
    t = t // 24

    day = t % 31 + 1
    t = t // 31

    month = t % 12 + 1
    t = t // 12

    year = t + 2000

    d = datetime(year, month, day, hour, minute, second)

    return d


def connect_command():
    """Start a connection with the time clock"""
    command = CMD_CONNECT
    command_bytes = b''
    chksum = 0
    session_id = 0
    reply_id = -1 + USHRT_MAX

    logger.debug("Connecting")
    return construct_header(
        command, chksum, session_id, reply_id, command_bytes)


def disable_device_command(reply):
    """Disable the timeclock. Show "Working..." on the clock and makes it
    unusable"""
    command = CMD_DISABLEDEVICE
    command_bytes = b'\x00\x00'
    chksum = 0
    session_id = unpack(b'HHHH', reply[:8])[2]
    reply_id = unpack(b'HHHH', reply[:8])[3]

    logger.debug("Disabling Device")
    return construct_header(
        command, chksum, session_id, reply_id, command_bytes)


def auth_command(reply, password):
    """Authenticate with the timeclock."""
    command = CMD_AUTH
    chksum = 0
    session_id = unpack(b'HHHH', reply[:8])[2]
    reply_id = unpack(b'HHHH', reply[:8])[3]
    command_bytes = make_commkey(password, session_id)

    logger.debug("Authenticating with device")
    return construct_header(
        command, chksum, session_id, reply_id, command_bytes)


def get_version_command(reply):
    """Get the version from the clock."""
    command = CMD_GET_VERSION
    command_bytes = b''
    chksum = 0
    session_id = unpack(b'HHHH', reply[:8])[2]
    reply_id = unpack(b'HHHH', reply[:8])[3]

    logger.debug("Getting version")
    return construct_header(
        command, chksum, session_id, reply_id, command_bytes)


def enable_device_command(reply):
    """Enable the timeclock. Makes the clock work again, after disabling it"""
    command = CMD_ENABLEDEVICE
    command_bytes = b''
    chksum = 0
    session_id = unpack(b'HHHH', reply[:8])[2]
    reply_id = unpack(b'HHHH', reply[:8])[3]

    logger.debug("Enabling Device")
    return construct_header(
        command, chksum, session_id, reply_id, command_bytes)


def disconnect_command(reply):
    """Disconnect from the clock"""
    command = CMD_EXIT
    command_bytes = b''
    chksum = 0
    session_id = unpack(b'HHHH', reply[:8])[2]
    reply_id = unpack(b'HHHH', reply[:8])[3]

    logger.debug("Disconnecting")
    return construct_header(
        command, chksum, session_id, reply_id, command_bytes)


def get_user_data_command(reply, data_type=FCT_USER):
    """Gets the timeclock to send the user data

    This command also seems to be used for some other things.
    It takes an optional second argument:
    FCT_USER (default)
    FCT_ATTLOG
    FCT_FINGERTMP
    FCT_OPLOG
    FCT_SMS
    FCT_UDATA
    FCT_WORKCODE
    """
    command = CMD_USERTEMP_RRQ
    command_bytes = pack(b'B', data_type)
    chksum = 0
    session_id = unpack(b'HHHH', reply[:8])[2]
    reply_id = unpack(b'HHHH', reply[:8])[3]

    logger.debug("Getting User Data")
    return construct_header(
        command, chksum, session_id, reply_id, command_bytes)


def get_attendance_log_command(reply):
    """gets the timeclock to send the attendance log"""
    command = CMD_ATTLOG_RRQ
    command_bytes = b''
    chksum = 0
    session_id = unpack(b'HHHH', reply[:8])[2]
    reply_id = unpack(b'HHHH', reply[:8])[3]

    logger.debug("Getting Attendance Log")
    return construct_header(
        command, chksum, session_id, reply_id, command_bytes)


def get_time_command(reply):
    """Get the time from the machine"""
    command = CMD_GET_TIME
    command_bytes = b''
    chksum = 0
    session_id = unpack(b'HHHH', reply[:8])[2]
    reply_id = unpack(b'HHHH', reply[:8])[3]

    logger.debug("Getting Time")
    return construct_header(
        command, chksum, session_id, reply_id, command_bytes)


def set_time_command(reply, time):
    """Set the time on the machine"""
    command = CMD_SET_TIME
    command_bytes = pack(b'I', encode_time(time))
    chksum = 0
    session_id = unpack(b'HHHH', reply[:8])[2]
    reply_id = unpack(b'HHHH', reply[:8])[3]

    logger.debug("Setting Time to {t}".format(t=time))
    return construct_header(
        command, chksum, session_id, reply_id, command_bytes)


def refreshdata_command(reply):
    """Refresh the configuration data"""
    command = CMD_REFRESHDATA
    command_bytes = b''
    chksum = 0
    session_id = unpack(b'HHHH', reply[:8])[2]
    reply_id = unpack(b'HHHH', reply[:8])[3]

    logger.debug("Refreshing configuration data")
    return construct_header(
        command, chksum, session_id, reply_id, command_bytes)


def check_ack_ok(reply):
    """Checks a returned packet to see if it returned CMD_ACK_OK,
    indicating success"""
    command = unpack(b'HHHH', reply[:8])[0]
    if command == CMD_ACK_OK:
        logger.debug("Success")
        return True
    else:
        logger.debug("Failure")
        return False


def check_ack_unauth(reply):
    """Checks a returned packet to see if it returned CMD_ACK_UNAUTH,
    indicating that a password will be needed to connect"""
    command = unpack(b'HHHH', reply[:8])[0]
    if command == CMD_ACK_UNAUTH:
        logger.debug("Unauthorized")
        return True
    else:
        return False


def check_ack_data(reply):
    """Checks a returned packet to see if it returned CMD_ACK_DATA,
    indicating a data packet"""
    command = unpack(b'HHHH', reply[:8])[0]
    if command == CMD_ACK_DATA:
        logger.debug("Data is going to be sent")
        return True
    else:
        logger.debug("Failure, data is not going to be sent")
        return False


def check_prepare_data(reply):
    """Checks a returned packet to see if it returned CMD_PREPARE_DATA,
    indicating that data packets are to be sent

    Returns the amount of bytes that are going to be sent"""
    command = unpack(b'HHHH', reply[:8])[0]
    if command == CMD_PREPARE_DATA:
        logger.debug("Data is going to be sent")
        logger.debug(len(reply[8:]))
        size = unpack(b'I', reply[8:12])[0]
        logger.debug("{n} bytes will be sent".format(n=size))
        return size
    else:
        logger.debug("Failure, data is not going to be sent")
        return False


def user_write_command(reply, rawdata):
    """Write some user data to the machine"""
    command = CMD_USER_WRQ
    command_bytes = rawdata
    chksum = 0
    session_id = unpack(b'HHHH', reply[:8])[2]
    reply_id = unpack(b'HHHH', reply[:8])[3]

    logger.debug("Uploading user temp data")
    return construct_header(
        command, chksum, session_id, reply_id, command_bytes)


def usertemp_write_command(reply, rawdata):
    """Write some usertemp data to the machine"""
    command = CMD_USERTEMP_WRQ
    command_bytes = rawdata
    chksum = 0
    session_id = unpack(b'HHHH', reply[:8])[2]
    reply_id = unpack(b'HHHH', reply[:8])[3]

    logger.debug("Uploading user temp data")
    return construct_header(
        command, chksum, session_id, reply_id, command_bytes)


def connect(host, port, password=""):
    """Connects to the timeclock and authenticates if it needs to.

    Returns the socket and result needed for passing to other commands.
    """
    # Connect to the timeclock
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect((host, int(port)))
    s.send(connect_command())
    r = s.recv(8)
    logger.debug(r)
    if check_ack_unauth(r):  # Do we need to authenticate
        s.send(auth_command(r, password))
        r = s.recv(8)
        logger.debug(r)
        check_ack_ok(r)

    return s, r


def get_version(s, last_reply):
    s.send(get_version_command(last_reply))
    r = s.recv(16)
    logger.debug(r)
    check_ack_ok(r)
    version = r[8:]
    return r, version


def disable(s, last_reply):
    # Disable the timeclock
    s.send(disable_device_command(last_reply))
    r = s.recv(8)
    logger.debug(r)
    check_ack_ok(r)
    return r


def enable(s, last_reply):
    # Enable the timeclock
    s.send(enable_device_command(last_reply))
    r = s.recv(8)
    logger.debug(r)
    check_ack_ok(r)
    return r


def disconnect(s, last_reply):
    # Disconnect from the timeclock
    s.send(disconnect_command(last_reply))
    r = s.recv(8)
    logger.debug(r)
    check_ack_ok(r)
    s.close()
    return r


def set_time(s, last_reply, dt):
    # Set the time
    s.send(set_time_command(last_reply, dt))
    r = s.recv(1032)
    logger.debug(r)
    check_ack_ok(r)
    return r


def get_time(s, last_reply):
    # Ask for time
    s.send(get_time_command(last_reply))
    r = s.recv(1032)
    logger.debug(r)
    check_ack_ok(r)
    return r, decode_time(unpack(b'I', r[8:])[0])


def user_write(s, last_reply, userdata):
    s.send(user_write_command(last_reply, userdata))
    r = s.recv(1024)
    logger.debug(r)
    check_ack_ok(r)
    return r


def refresh_config_data(s, last_reply):
    # Refresh the configuration data
    s.send(refreshdata_command(last_reply))
    r = s.recv(8)
    logger.debug(r)
    check_ack_ok(r)
    return r


def get_user_data(s, last_reply):
    # Get User data from the timeclock
    s.send(get_user_data_command(last_reply))
    r = s.recv(16)
    logger.debug(r)
    userdata = []
    if check_prepare_data(r):
        bytes_ = check_prepare_data(r)
        while bytes_ > 0:
            # Receive 1024 bytes of data + 8 byte header
            nr = s.recv(1032)
            logger.debug(nr)
            userdata.append(nr[8:])
            bytes_ -= 1024

    # after the data is sent, another packet is sent, indicating success
    sr = s.recv(8)
    logger.debug(sr)
    check_ack_ok(sr)
    return r, userdata


def get_log_data(s, last_reply):
    # Get the attendance log data
    s.send(get_attendance_log_command(last_reply))
    r = s.recv(32)
    logger.debug(r)

    logdata = []
    bytes_ = check_prepare_data(r)
    if bytes_:
        while bytes_ > 0:
            # Receive 1024 bytes of data plus 4 byte header
            # The attendance log gets cut off at the end, but the next
            # entry is the continuation of it.
            nr = s.recv(1032)
            logger.debug(nr)
            logdata.append(nr[8:])
            bytes_ -= 1024

    # after the data is sent, another packet is sent, indicating success
    sr = s.recv(8)
    logger.debug(sr)
    check_ack_ok(sr)

    return r, logdata


class User(object):
    def __init__(
            self, uid=0, privilege=0, password="",
            name="", card=0, group=0, timezone=0, uid2=0):
        self.uid = uid
        self.privilege = privilege
        self.password = password
        self.name = name
        self.card = card
        self.group = group
        self.timezone = timezone
        self.uid2 = uid2


def unpack_user(userdata):
    """Takes a user data structure from the time and attendance clock
    and unpacks it to this object"""
    user = User()
    user.uid = unpack(b'H', userdata[:2])[0]
    user.privilege = unpack(b'B', userdata[2:3])[0]
    user.password = unpack(b'5s', userdata[3:8])[0]
    user.name = unpack(b'8s', userdata[8:16])[0].split(b'\0')[0]
    user.card = unpack(b'5s', userdata[16:21])[0]
    user.group = unpack(b'B', userdata[21:22])[0]
    user.timezone = unpack(b'H', userdata[22:24])[0]
    user.uid2 = unpack(b'I', userdata[24:28])[0]
    return user


def pack_user(user):
    userdata = pack(b'H', user.uid)
    userdata += pack(b'B', user.privilege)
    userdata += pack(b'5s', user.password)
    userdata += pack(b'8s', user.name.encode('utf-8'))
    userdata += pack(b'5s', user.card)
    userdata += pack(b'B', user.group)
    userdata += pack(b'H', user.timezone)
    userdata += pack(b'I', user.uid2)
    return userdata


def unpack_users_to_dict(usersdata):
    users = {}
    while (len(usersdata) / 28) > 0:
        user = unpack_user(usersdata[:28])
        users[user.uid] = user
        usersdata = usersdata[28:]

    return users


def combine_user_packets(userdata_packets):
    userdata = b''.join(userdata_packets)

    return userdata[4:]


class UserTemp(object):
    def __init__(self, size=0, pin=0, finger_id="", valid="", data=0):
        self.size = size
        self.pin = pin
        self.finger_id = finger_id
        self.valid = valid
        self.data = data


def unpack_usertemp(usertempdata):
    """Takes a user fingerprint template data structure from the time and
    attendance clock and returns it in a UserTemp object"""
    usertemp = UserTemp()
    usertemp.size = unpack(b'H', usertempdata[:2])[0]
    usertemp.pin = unpack(b'H', usertempdata[2:4])[0]
    usertemp.finger_id = unpack(b'B', usertempdata[4:5])[0]
    usertemp.valid = unpack(b'B', usertempdata[5:6])[0]
    usertemp.data = unpack(
        '{0}s'.format(usertemp.size - 6).encode('utf-8'),
        usertempdata[6:usertemp.size])[0]
    return usertemp


def pack_usertemp(usertemp):
    """Takes a UserTemp object and packs it into a data string suitable to
    send back to the time and attendance machine"""
    usertempdata = pack(b'H', len(usertemp.data) + 6)
    usertempdata += pack(b'H', usertemp.pin)
    usertempdata += pack(b'B', usertemp.finger_id)
    usertempdata += pack(b'B', usertemp.valid)
    usertempdata += pack(bytes(usertemp.size - 6) + b's', usertemp.data)
    return usertempdata


def unpack_usertemps_to_list(usertempsdata):
    usertemps = []
    while (len(usertempsdata)) > 6:
        usertemp = unpack_usertemp(usertempsdata)
        usertemps.append(usertemp)
        usertempsdata = usertempsdata[usertemp.size:]

    return usertemps


def combine_usertemp_packets(usertemp_data_packets):
    u = []
    for packet in usertemp_data_packets:
        u.append(packet)

    usertempdata = "".join(u)

    return usertempdata[4:]


verification_lookup = {
    0:  "Check In (Code)",
    8:  "Check In (Fingerprint)",
    32: "Check Out (Code)",
    40: "Check Out (Fingerprint)",
}


class Log(object):
    def __init__(
            self,
            uid,
            verified,
            time,
            status=None,
            reserved=None,
            workcode=None,
            uid2=None):
        self.uid = uid
        self.verified = verified
        self.time = time
        self.status = status
        self.reserved = reserved
        self.workcode = workcode
        self.uid2 = uid2

    def decode_verified(self):
        try:
            return verification_lookup[self.verified]
        except KeyError:
            return "Unkown verification method: %s" % self.verified


def unpack_log(logdata):
    uid, verified, time = unpack(b'HHI', logdata)
    return Log(uid, verified, time)


def unpack_log_ssr(logdata):
    uid, uid2, verified, time, workcode, reserved = unpack(
        b'H24sHII4B', logdata)
    return Log(
        uid, verified, time, uid2=uid2, workcode=workcode, reserved=reserved)


def strip_headers(data_packets):
    stripped_data_packets = []
    for i in range(len(data_packets)):
        stripped_data_packets.append(data_packets[i][4:])
    return stripped_data_packets


def join_log_data_packets(log_data_packets):
    joined_log_data_packets = log_data_packets.pop(0)
    for i in range(len(log_data_packets)):
        joined_log_data_packets += log_data_packets[i][4:]
    return joined_log_data_packets


def assemble_log_data_from_packets(log_data_packets):
    log_data_packets = strip_headers(log_data_packets)
    return join_log_data_packets(log_data_packets)
