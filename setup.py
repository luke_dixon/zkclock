from setuptools import setup

setup(
    name='zkclock',
    version='0.3',
    author='Luke Dixon',
    author_email='6b8b4567@gmail.com',
    packages=['zkclock'],
    test_suite='zkclock_test',
)
